#### CLI ENG-RUS, RUS-ENG translator

Requires `php` and `node js` installed

To check if it runs ok, execute `run` script to check if all dependencies are met

Default translation direction is `en-ru`

You can specify translation direction manually

```bash
./run ru-en
# or
./run en-ru
```
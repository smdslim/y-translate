<?php

# Example: 
# php index.php --phrase=foo

require_once ('vendor/autoload.php');
use \Dejurin\GoogleTranslateForFree;

$options = [
    'phrase:',      // required
    'source::',     // optional
    'target::',     // optional
    'attempts::'    // optional
];

$options = getopt('', $options);

if(!isset($options['phrase'])) die("Phrase not specified\r\n");

$source = @$options['source'] ?: 'en';
$target = @$options['target'] ?: 'ru';
$attempts = @$options['attempts'] ?: 5;
$phrase = $options['phrase'];

$tr = new GoogleTranslateForFree();
$result = $tr->translate($source, $target, $phrase, $attempts);

echo "$result";

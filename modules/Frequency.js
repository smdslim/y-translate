const axios = require('axios');
const fs = require('fs');
const config = require('../config');
const { exec } = require('child_process');

class Frequency {
    async preserveFrequency(phrase, translator) {

        if (translator.source_lang == 'ru') return;
        if (config.GET_FREQUENCY === false) return;

        this.preserveCollinsFrequency(phrase);
        this.preserveEnglishCorporaFrequency(phrase);

    }

    async preserveEnglishCorporaFrequency(phrase) {

        fs.writeFileSync(config.FREQUENCY_ENGLISH_CORPORA_FILE_PATH, JSON.stringify({ phrase: phrase, frequency: '...' }));

        exec(`phantomjs ${config.PROJECT_PATH}/modules/phantom/EnglishCorporaPage.js --text="${phrase}"`, (error, stdout, stderr) => {
            if (error) console.log(error);
            const result = {
                phrase: phrase,
                frequency: stdout.replace('\n', '')
            };
            fs.writeFile(config.FREQUENCY_ENGLISH_CORPORA_FILE_PATH, JSON.stringify(result, null, 4), (err) => {
                if (err) console.log(err);
            });
        });

    }

    async preserveCollinsFrequency(phrase) {

        try {
            const { data } = await axios.get(`https://bot.glabs.su/api/translator_bot/word_frequency/${phrase}`);

            let result = {
                phrase: phrase,
                frequency: '...'
            };

            /**
             * Success
             */
            if (data.success) {
                result.frequency = data.frequency;
                return fs.writeFile(config.FREQUENCY_COLLINS_FILE_PATH, JSON.stringify(result, null, 4), (err) => {
                    if (err) console.log(err);
                });
            }

            /**
             * Error
             */
            fs.writeFile(config.FREQUENCY_COLLINS_FILE_PATH, JSON.stringify(result, null, 4), (err) => {
                if (err) console.log(err);
            });
        } catch (e) {
            
        }

    }
}

module.exports = new Frequency();
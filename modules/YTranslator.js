const config = require('./../config');
const axios = require('axios');
const parser = require('./Parser');
const table = require('./Table');
const { exec } = require('child_process');

const directions = ['en-ru', 'ru-en'];

class YTranslator {

    constructor(direction) {

        this.translation = '';
        this.translationObject = {};

        this.direction = 'en-ru';
        this.source_lang = 'en';
        this.target_lang = 'ru';

        this.setDirection(direction);
    }

    setDirection(direction) {
        if (!direction) return;
        if (!directions.includes(direction)) return;
        this.direction = direction;

        const st = direction.split('-'); // source-target
        this.source_lang = st[0];
        this.target_lang = st[1];
    }

    async getTranslation(phrase) {
        return new Promise((resolve, reject) => {
            exec(`${config.PROJECT_PATH}/google-translate-free/translate --phrase="${phrase}" --source=${this.source_lang} --target=${this.target_lang}`, (error, stdout, stderr) => {
                if (error) return reject(error);
                this.translation = stdout;
                return resolve(this.translation);
            });
        })
    }

    async getTranslationObject(phrase) {
        const url = config.AT_URL.replace(config.PHRASE_PLACEHOLDER, encodeURIComponent(phrase)).replace(config.DIRECTION_PLACEHOLDER, this.direction);

        try {
            const {data} = await axios.get(url);
            this.translationObject = data;
            return data;
        } catch (e) {
            console.error('Could nod get translation object', e.message)
            return false;
        }
    }

    async printSingle(phrase) {
        await this.getTranslation(phrase);
        if (this.translation) console.log(this.translation);
    }

    async printTable(phrase) {
        await this.getTranslationObject(phrase);
        const result = parser.parseRemoteData(this.translationObject, this.direction);

        if (this.source_lang == 'en') console.log(result.transcription);

        if (!config.SHOW_TRANSLATIONS) return;
        table.render(result)
    }

}

module.exports = YTranslator;
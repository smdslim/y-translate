const CliTable = require('cli-table');

class Table {
    render(result) {

        const tKeys = Object.keys(result.translations)
        const sKeys = Object.keys(result.synonyms)

        if (!tKeys.length) return;

        const table = new CliTable({
            head: [
                'Translations',
                'Synonyms',
            ],
            colWidths: [30, 30]
        });

        for (let i = 0; i < result.totalRowsAmount; i++) {
            let row = ['', ''];

            if (tKeys[i]) {
                row[0] = result.translations[tKeys[i]].join("\n");
            }
            if (sKeys[i]) {
                row[1] = result.synonyms[sKeys[i]].join("\n");
            }

            table.push(row);
        }

        console.log(table.toString())
    }
}

module.exports = new Table();
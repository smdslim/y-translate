let object = {
    "jsonrpc": "2.0",
    "method": "LMT_handle_jobs",
    "params": {
        "jobs": [
            {
                "kind": "default",
                "raw_en_sentence": "убить",
                "raw_en_context_before": [],
                "raw_en_context_after": [],
                "quality": "fast"
            }
        ],
        "lang": {
            "user_preferred_langs": [
                "EN",
                "RU"
            ],
            "source_lang_user_selected": "EN",
            "target_lang": "RU"
        },
        "priority": -1,
        "timestamp": 0
    },
    "id": 25630018
}

module.exports = (settings) => {
    if (!settings.source_lang || !settings.target_lang || !settings.phrase || !settings.id)
        throw new Error('Not all required parameters passed in');

    object.params.lang.source_lang_user_selected = settings.source_lang.toUpperCase();
    object.params.lang.target_lang = settings.target_lang.toUpperCase();
    object.params.jobs[0].raw_en_sentence = settings.phrase;
    object.params.timestamp = Date.now();
    object.id = settings.id;

    return object;
}
const config = require('./../config');
const c = require('../constants');
const axios = require('axios')

let Translator;

switch (config.TRANSLATE_HANDLER) {
    case c.TRANSLATE_HANDLER_GOOGLE:
        Translator = require('./GTranslator');
        break;
    case c.TRANSLATE_HANDLER_DEEPL:
        Translator = require('./DTranslator');
        break;
    default:
    case c.TRANSLATE_HANDLER_YANDEX:
        Translator = require('./YTranslator');
        break;
}

class Translation {

    constructor(direction) {
        this.translator = new Translator(direction);
    }

    async printSingle(phrase) {
        await this.translator.printSingle(phrase);
        try {
            axios.post(config.API_HISTORY_ENDPOINT, {
                'phrase': decodeURIComponent(phrase),
                'translation': this.translator.translation || 'No translation'
            })
        } catch (e) {
            console.log('Could not save phrase to history');
        }
    }

    async printTable(phrase) {
        await this.translator.printTable(phrase);
    }

}

module.exports = Translation;
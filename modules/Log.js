const fs = require('fs');
config = require('../config');

module.exports = {
    info: (data, file_name) => {
        if (!file_name) file_name = 'info.log';
        path = `${config.PROJECT_PATH}/logs/${file_name}`
        fs.writeFileSync(path, data);
    }
}
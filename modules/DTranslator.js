/*
*    Translation class using deepl.com
 */
const config = require('./../config');
const request_structure = require('./partials/deepl_structure');
const axios = require('axios');
const directions = ['en-ru', 'ru-en'];

class DTranslator {
    constructor(direction) {
        this.request_id = 80350001;
        this.translation = '';
        this.translationObject = {};
        this.direction = 'en-ru';
        this.source_lang = 'en';
        this.target_lang = 'ru';
        this.setDirection(direction);
    }

    setDirection(direction) {
        if (!direction) return;
        if (!directions.includes(direction)) return;
        this.direction = direction;

        const st = direction.split('-'); // source-target
        this.source_lang = st[0];
        this.target_lang = st[1];
    }

    async getTranslation(phrase) {
        try {
            this.request_id++;
            const request_data = request_structure({source_lang: this.source_lang, target_lang: this.target_lang, phrase: phrase, id: this.request_id});
            console.log('me:', JSON.stringify(request_data));
            const {data} = await axios.post(
                config.D_URL,
                request_data,
                {headers: {'User-Agent': config.USER_AGENT_HEADER}}
            )
            if (data.code == 200) {
                this.translation = data.text.join(' ');
                return this.translation;
            }
            return false;
        } catch (e) {
            console.log('Could not get single word translation', e.message);
            return false;
        }
    }

    async printSingle(phrase) {
        await this.getTranslation(phrase);
        if (this.translation) console.log("-------------\n" + this.translation + "\n-------------");
    }

}

module.exports = DTranslator;

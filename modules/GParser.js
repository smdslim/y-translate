class GParser {

    /**
     * Translations and synonyms at index 1
     * Examples at index 12
     */
    parseTranslations(data, direction) {

        let result = {
            translations: {},
            explanations: {}
        }

        if (data[1]) {
            for (let value of data[1]) {
                if (!result.translations[value[0]]) result.translations[value[0]] = {};
                for (let translation of value[2]) {
                    if (!result.translations[value[0]][translation[0]]) result.translations[value[0]][translation[0]] = translation[1].join(', ');
                }
            }
        }

        if (data[12]) {
            for (let value of data[12]) {
                if (!result.explanations[value[0]]) result.explanations[value[0]] = [];
                for (let translation of value[1]) {
                    result.explanations[value[0]].push({
                        explanation: translation[0],
                        example: translation[2]
                    })
                }
            }
        }

        return result;
    }
}

module.exports = new GParser();
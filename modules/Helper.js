const c = require('../constants');

class Helper {
    encodePhrase(phrase, source_lang) {
        if (source_lang == c.LANG_EN) {
            phrase = phrase.replace(/[а-яА-ЯЁё]/g, '');
        }
        return encodeURIComponent(phrase);
    }
}


module.exports = new Helper();
require('custom-env').env();

const config = require('../config');
const CliTable = require('cli-table');

class GTable {
    render(data) {

        const table = new CliTable({
            head: [
                'Translations',
                'Synonyms',
            ],
            colWidths: [30, 40]
        });

        if (config.SHOW_TRANSLATIONS && Object.keys(data.translations).length > 0) {
            for (let key in data.translations) {
                for (let translation in data.translations[key]) {
                    let row = ['', ''];
                    row[0] = translation;
                    row[1] = data.translations[key][translation];
                    table.push(row);
                }
            }

            console.log(table.toString())
        }

        if (config.SHOW_EXPLANATIONS && Object.keys(data.explanations).length > 0) {
            const e_table = new CliTable({
                head: [
                    'Explanation',
                    'Example',
                ],
                colWidths: [40, 40]
            });

            const a = new Array(35).fill('.');
            const r = new RegExp(`(${a.join('')})`, 'g')

            for (let key in data.explanations) {
                for (let value of data.explanations[key]) {
                    let row = [
                        (value.explanation || '').replace(r, "$1\n"),
                        (value.example || '').replace(r, "$1\n")
                    ]
                    e_table.push(row);
                }
            }
            console.log(e_table.toString())
        }
    }

}

module.exports = new GTable();
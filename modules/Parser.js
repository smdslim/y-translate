const Log = require('./Log')

class Parser {

    parseRemoteData(data, direction) {

        let result = {
            totalRowsAmount: 0,
            translations: {},
            synonyms: {},
            transcription: null
        }

        this.parseTranslation(data, result, direction);
        this.parseSynonyms(data, result);

        return result;
    }

    parseTranslation(data, result, direction) {
        let rowsAmount = 0

        for (let item of data[direction]['regular']) {
            if (!item['pos']) continue;
            if (!result.transcription) result.transcription = item['ts'];
            if (!result.translations[item['pos']['text']]) {
                result.translations[item['pos']['text']] = [];
                rowsAmount++
                if (rowsAmount > result.totalRowsAmount) result.totalRowsAmount = rowsAmount
            }
            for (let tr of item['tr']) {
                result.translations[item['pos']['text']].push(tr['text'])
            }
        }
        return result;
    }

    parseSynonyms(data, result) {
        let rowsAmount = 0

        for (let item of data['en']['syn']) {
            for (let tr of item['tr']) {
                if (!tr['pos']) continue;
                if (!result.synonyms[tr['pos']['text']]) {
                    result.synonyms[tr['pos']['text']] = [];
                    rowsAmount++
                    if (rowsAmount > result.totalRowsAmount) result.totalRowsAmount = rowsAmount
                }
                result.synonyms[tr['pos']['text']].push(tr['text'])
            }
        }
        return result;
    }

}

module.exports = new Parser();
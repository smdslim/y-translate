const config = require('./../config');
const axios = require('axios');
const parser = require('./GParser');
const table = require('./GTable');
const log = require('./Log');
const c = require('../constants');
const { exec } = require('child_process');

class GTranslation {

    constructor(direction) {

        this.translation = '';
        this.translationObject = {};

        this.source_lang = 'en';
        this.target_lang = 'ru';

        this.setDirection(direction);

    }

    setDirection(direction) {
        if (!direction) return;
        const st = direction.split('-'); // source-target

        if (st.length != 2) return;
        this.source_lang = st[0];
        this.target_lang = st[1];
    }

    /**
     * Get single word translation and store full translation object
     * @param phrase
     * @returns {Promise<*>}
     */
    async getTranslation(phrase) {
        const url = await this.getTranslationUrl(phrase);
        try {
            const request_config = {
                headers: { 'User-Agent': config.USER_AGENT_HEADER }
            };
            const { data } = await axios.get(url, request_config);
            this.translationObject = data;
            this.translation = data[0][0][0];

            log.info(JSON.stringify(data, undefined, 4), 'response.json');

            return this.translation;
        } catch (e) {
            console.log('Could not get single word translation', e);
            return false;
        }
    }

    async printSingle(phrase) {
        await this.getTranslation(phrase);
        if (this.translation) console.log("-------------\n" + this.translation + "\n-------------");
    }

    async getTranslationUrl(phrase) {
        return new Promise((resolve, reject) => {
            exec(`phantomjs ${config.PROJECT_PATH}/modules/phantom/GTranslatePage.js --sl=${this.source_lang} --tl=${this.target_lang} --text='${phrase}'`, (error, stdout, stderr) => {
                if (error) return reject(error);
                return resolve(stdout);
            });
        })
    }

    async printTable(phrase) {
        const result = parser.parseTranslations(this.translationObject);
        table.render(result)
    }

}

module.exports = GTranslation;
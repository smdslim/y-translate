const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});
const Translation = require('./Translation');
const image = require('./Image');
const audio = require('./Audio');
const frequency = require('./Frequency');
const helper = require('./Helper');

class Main {

    constructor(direction) {
        this.translation = new Translation(direction);
    }

    ask() {
        readline.question('Enter phrase: ', (phrase) => {
            this.translate(phrase);
        })
    }

    async translate(phrase) {

        // phrase = helper.encodePhrase(phrase, this.translation.translator.source_lang);

        await this.translation.printSingle(phrase);

        await this.translation.printTable(phrase);

        await audio.play(phrase, { lang: this.translation.translator.source_lang });

        await image.show(phrase);

        frequency.preserveFrequency(phrase, this.translation.translator);

        this.ask();
    }
}

module.exports = Main;
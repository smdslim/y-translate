var page = require('webpage').create();
var system = require('system');
var translate_data = {
    'sl': 'en',
    'tl': 'ru',
    'text': false
};

parseArgs(system.args);

if (!translate_data.text) {
    console.log('No phrase to translate passed');
    phantom.exit();
}

page.open(url(translate_data), function () {
    phantom.exit();
});

page.onResourceReceived = function (response) {
    if (response.contentType == 'application/json; charset=UTF-8' && response.stage == 'end') {
        console.log(response.url);
    }
};


function parseArgs(args) {
    var available_keys = [
        '--sl', '--tl', '--text'
    ];
    args.forEach(function (arg) {
        var values = arg.split('=');
        var key = values[0];
        var value = values[1];
        if (available_keys.indexOf(key) != -1 && value) translate_data[key.replace('--', '')] = value;
    });
}

function url(data) {
    data.text = encodeURIComponent(data.text);
    return 'https://translate.google.com/#view=home&op=translate&sl=' + data.sl + '&tl=' + data.tl + '&text=' + data.text;
}
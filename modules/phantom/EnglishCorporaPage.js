var page = require('webpage').create();
var system = require('system');
var args = parseArgs(system.args);

if (!args.text) {
    console.log('Text not passed in');
    phantom.exit(1);
}

page.viewportSize = {
    width: 800,
    height: 600
};

var base_url = 'https://www.english-corpora.org/coca';
var max_retry_amount = 10;
var total_tries_amount = 15;

page.open(base_url, function (status) {

    if (status === "success") {
        page.evaluate(function (phrase) {

            var f = window.frames[2].document;

            f.getElementById('p').value = phrase;

            setTimeout(function () {
                f.getElementById('submit1').click();
            }, 1000)

        }, args.text);

    } else {
        phantom.exit(1);
    }
});


page.onLoadFinished = function () {

    var try_amount = 0;
    var total_tries = 0;

    setInterval(function () {

        if (total_tries >= total_tries_amount) {
            console.log('-2');
            phantom.exit();
        }
        var result = page.evaluate(function () {
            try {
                return window.frames[3].document.querySelector('.auto-style1 > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4) > font:nth-child(1)').innerText;
            } catch (e) {

            }
        });
        result = parseInt(result);
        if (!isNaN(result)) {
            if (try_amount >= max_retry_amount) {
                console.log('-1');
                phantom.exit();
            }
            if (parseInt(result)) {
                console.log(result);
                phantom.exit();
            }
            try_amount++;
        }
        total_tries++;
    }, 300)
};

page.onError = function (error) {
    // console.log(error)
}

function parseArgs(args) {
    var available_keys = [
        '--text'
    ];
    var result = {};
    args.forEach(function (arg) {
        var values = arg.split('=');
        var key = values[0];
        var value = values[1];
        if (available_keys.indexOf(key) != -1 && value) result[key.replace('--', '')] = value;
    });
    return result;
}
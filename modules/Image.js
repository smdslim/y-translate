require('custom-env').env();

const { exec } = require('child_process');
const config = require('./../config');
const axios = require('axios');
const https = require('https');
const fs = require('fs');

class Image {

    constructor() {
        this.store_path = `${config.PROJECT_PATH}/resources/${config.PHRASE_IMAGE_FILE_NAME}`;
        this.preview_title = 'yt_image';
    }

    async show(phrase) {

        await this.killFehInstances();

        if (!config.SHOW_PICTURE) return;

        if (!config.SHOW_IMAGE_PREVIEW) return;

        try {
            await this.downloadImage(encodeURIComponent(phrase), this.store_path);
            exec(`feh ${this.store_path} --title ${this.preview_title}`);
        } catch (e) {
            console.error('Could not get image for the phrase', e.message);
        }
    }

    /**
     * Kill all image preview instances initiated with `feh` app
     * @returns {Promise<*>}
     */
    async killFehInstances() {
        return new Promise((resolve) => {
            exec(`ps ax | grep ${this.preview_title} | grep -v grep | awk '{print $1}'`, (err, stdout, stderr) => {
                let p_ids = [];
                if (stdout) {
                    p_ids = stdout.split("\n").filter(item => item);
                    for (let p_id of p_ids) exec(`kill -15 ${p_id}`);
                }
                resolve(true);
            });
        })
    }

    async downloadImage(phrase, path) {
        const image_url = await this.getPhraseImageUrl(phrase);

        return new Promise((resolve, reject) => {
            if (!image_url) return reject('No image URL found');
            https.get(image_url, {}, (response) => {
                response.setEncoding('binary')
                let result = []
                response.on('data', (chunk) => {
                    result.push(chunk)
                })
                response.on('end', () => {
                    fs.writeFileSync(path, result.join(''), 'binary')
                    resolve(true)
                })
            }).on('error', (error) => {
                reject(error)
            })
        })
    }

    async getPhraseImageUrl(phrase) {
        const url = config.I_URL.replace(config.PHRASE_PLACEHOLDER, phrase);
        const { data } = await axios.get(url);

        if (data[0]) {
            let image_url;
            for (let dimention in data[0].meanings[0].images) {
                image_url = 'https:' + data[0].meanings[0].images[dimention].url;
                break;
            }
            return image_url;
        }
        return null;
    }
}

module.exports = new Image();
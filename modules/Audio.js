const config = require('./../config');
const https = require('https');
const fs = require('fs');
const { exec } = require('child_process');

class Audio {
    async download(phrase, options) {

        const url = config.AUDIO_URL.replace(config.PHRASE_PLACEHOLDER, phrase).replace(config.LANG_PLACEHOLDER, options.lang);

        const r_config = {
            headers: {
                'Referer': 'http://translate.google.com',
                'User-Agent': 'stagefright/1.2 (Linux;Android 5.0)',
            }
        }

        return new Promise((resolve, reject) => {
            https.get(url, r_config, (response) => {

                response.setEncoding('binary');

                let result = [];

                response.on('data', (chunk) => {
                    result.push(chunk);
                })
                response.on('end', () => {
                    if (!options.path) {
                        options.path = `${config.PROJECT_PATH}/resources/${config.PHRASE_AUDIO_FILE_NAME}`;
                    }
                    fs.writeFileSync(options.path, result.join(''), 'binary')
                    resolve(true);
                })
            }).on('error', (error) => {
                console.error(error);
                reject();
            })
        })
    }

    async play(phrase, lang) {
        if (!config.PLAY_SOUND) return;
        await this.download(phrase, lang);
        exec(`play ${config.PROJECT_PATH}/resources/${config.PHRASE_AUDIO_FILE_NAME} &> /dev/null`);
    }
}

module.exports = new Audio();
const translation = require('./../modules/Translation');
const config = require('./../config');
const fs = require('fs');
const audioFilePath = `${config.PROJECT_PATH}/resources/audio.mp3`;
const audio = require('./../modules/Audio');

test('Yandex API, able to get phrase translation', () => {
    return translation.getTranslation('sex').then(response => expect(response).toEqual('пол'));
});


test('Translate object is receiving', () => {
    return translation.getTranslationObject('sex').then(response => expect(typeof response).toEqual('object'));
});

test('Translate object is not empty', async () => {
    const response = await translation.getTranslationObject('sex');
    expect(response['en-ru']).toBeDefined();
    expect(response['en']).toBeDefined();
});

test('Audio can be received', async () => {
    expect(audio.download('sex', { path: audioFilePath, lang: 'en' })).resolves.toBe(true);
});

test('Audio can be downloaded', async () => {
    if (fs.existsSync(audioFilePath)) fs.unlink(audioFilePath, (err) => { if (err) console.log(err) });
    await audio.download('sex', { path: audioFilePath, lang: 'en' });
    expect(fs.existsSync(audioFilePath)).toBeTruthy();
});
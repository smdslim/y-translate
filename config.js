require('custom-env').env();

const c = require('./constants');
const PHRASE_PLACEHOLDER = '%PHRASE%';
const DIRECTION_PLACEHOLDER = '%DIRECTION%';
const LANG_PLACEHOLDER = '%LANG%';
const YANDEX_SESSION_ID = process.env.YANDEX_SESSION_ID || 'a9cc282e.5c8acb2a.1b8d0129-0-0';
const GOOGLE_SESSION_ID = process.env.GOOGLE_SESSION_ID || '712852.816192';
// URL to get list of translations and synonyms
/**
 * Yandex endpoint to get synonyms and synonyms translations for the phrase
 * @type {string}
 */
const AT_URL = `https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=en&srv=tr-text&sid=20876ce8.5c7db159.c33f0ee2&text=${PHRASE_PLACEHOLDER}&dict=en.syn%2Cen.ant%2Cen.deriv%2C${DIRECTION_PLACEHOLDER}.regular&flags=103`;
/**
 * Yandex endpoint for single word translation
 * @type {string}
 */
const T_URL = `https://translate.yandex.net/api/v1/tr.json/translate?id=${YANDEX_SESSION_ID}&srv=tr-text&lang=${DIRECTION_PLACEHOLDER}&reason=paste`;
/**
 * SkyEng translation/image receive endpoint
 * @type {string}
 */
const I_URL = `https://dictionary.skyeng.ru/api/v2/search-word-translation?images=200x150&text=${PHRASE_PLACEHOLDER}`
/**
 * Google endpoint to receive phrase pronunciation
 * @type {string}
 */
const AUDIO_URL = `https://translate.google.com/translate_tts?ie=UTF-8&q=${PHRASE_PLACEHOLDER}&tl=${LANG_PLACEHOLDER}&client=tw-ob`;
/**
 * Google endpoint to receive word translation, sentences examples and synonyms
 * @type {string}
 */
const G_URL = `https://translate.google.ru/translate_a/single?client=webapp&sl=en&tl=ru&hl=en&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&source=bh&ssel=0&tsel=0&kc=1&tk=${GOOGLE_SESSION_ID}&q=${PHRASE_PLACEHOLDER}`;
/**
 * Deepl.com endpoint to receive phrase translations. POST
 * @type {string}
 */
const D_URL = `https://www2.deepl.com/jsonrpc`;

const USER_AGENT_HEADER = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.96 Safari/537.36';

const PHRASE_AUDIO_FILE_NAME = 'phrase.mp3';
const PHRASE_IMAGE_FILE_NAME = 'phrase.png';
const TRANSLATE_HANDLER = process.env.TRANSLATE_HANDLER || c.TRANSLATE_HANDLER_GOOGLE;
const SHOW_TRANSLATIONS = process.env.SHOW_TRANSLATIONS == 'true';
const SHOW_EXPLANATIONS = process.env.SHOW_EXPLANATIONS == 'true';
const SHOW_IMAGE_PREVIEW = process.env.SHOW_IMAGE_PREVIEW == 'true';
const SHOW_PICTURE = process.env.SHOW_PICTURE == 'true';
const PLAY_SOUND = process.env.PLAY_SOUND == 'true';
const GET_FREQUENCY = process.env.GET_FREQUENCY == 'true';
const FREQUENCY_COLLINS_FILE_PATH = '/tmp/w-c-freq';
const FREQUENCY_ENGLISH_CORPORA_FILE_PATH = '/tmp/w-ec-freq';

const API_HISTORY_ENDPOINT = process.env.API_HISTORY_ENDPOINT || 'http://smdslim.bot:8080/api/translator_bot/history/store';

module.exports = {
    AT_URL: AT_URL,
    T_URL: T_URL,
    I_URL: I_URL,
    G_URL: G_URL,
    D_URL: D_URL,
    PHRASE_PLACEHOLDER: PHRASE_PLACEHOLDER,
    DIRECTION_PLACEHOLDER: DIRECTION_PLACEHOLDER,
    LANG_PLACEHOLDER: LANG_PLACEHOLDER,
    PHRASE_AUDIO_FILE_NAME: PHRASE_AUDIO_FILE_NAME,
    AUDIO_URL: AUDIO_URL,
    PHRASE_IMAGE_FILE_NAME: PHRASE_IMAGE_FILE_NAME,
    PROJECT_PATH: __dirname,
    USER_AGENT_HEADER: USER_AGENT_HEADER,
    TRANSLATE_HANDLER: TRANSLATE_HANDLER,
    SHOW_TRANSLATIONS: SHOW_TRANSLATIONS,
    SHOW_EXPLANATIONS: SHOW_EXPLANATIONS,
    SHOW_IMAGE_PREVIEW: SHOW_IMAGE_PREVIEW,
    SHOW_PICTURE: SHOW_PICTURE,
    PLAY_SOUND: PLAY_SOUND,
    GET_FREQUENCY: GET_FREQUENCY,
    FREQUENCY_COLLINS_FILE_PATH: FREQUENCY_COLLINS_FILE_PATH,
    FREQUENCY_ENGLISH_CORPORA_FILE_PATH: FREQUENCY_ENGLISH_CORPORA_FILE_PATH,
    API_HISTORY_ENDPOINT: API_HISTORY_ENDPOINT
}
module.exports = {

    LANG_RU : 'ru',
    LANG_EN : 'en',

    TRANSLATE_HANDLER_GOOGLE : 'google',
    TRANSLATE_HANDLER_YANDEX : 'yandex',
    TRANSLATE_HANDLER_DEEPL : 'deepl',

}